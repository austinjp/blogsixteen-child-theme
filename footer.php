<?php
/**
 * The template for displaying the footer.
 * Contains the closing of the #content div and all content after.
 *
 * @package BlogSixteenChild
 */

?>

	</div><!-- #content -->

      </div><!-- #page -->


    <div id="colophon-push"></div>

  </div><!-- #wrap-all -->

  <footer id="colophon" class="site-footer" role="contentinfo">
    <div id="footer-content">
<?php
   if (function_exists(dynamic_sidebar('bsc-footer-3'))) {
     dynamic_sidebar('bsc-footer-3');
   }
?>
    </div>
  </footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
