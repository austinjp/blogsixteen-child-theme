<?php
/**
 * @link https://bitbucket.org/austinjp/blogsixteen-child
 *
 * @package BlogSixteenChild
 */


function blogsixteen_posted_in($prefix = 'Posted in') {
    $categories_list = get_the_category_list( esc_html__( ', ', 'blogsixteen' ) );
    if ( $categories_list && blogsixteen_categorized_blog() ) {
        printf( '<span class="cat-links">' . esc_html__( $prefix . ' %1$s', 'blogsixteen' ) . '</span>', $categories_list ); // WPCS: XSS OK.
    }
}

/* Fancy footer. Courtesy http://mekshq.com/how-to-add-last-class-to-wordpress-footer-widget/ */
function bsc_footer_widgets($params) {
    global $widget_num;

    if(isset($params[0]['id']) && preg_match('/^(bsc\-footer\-)(\d+)/i', $params[0]['id'], $max_specified)) {

	$max_per_row = isset($max_specified[2]) ? $max_specified[2] : 1;

	$widget_num++;
	
	$col = $widget_num % $max_per_row;
	if ($col === 0) { $col = $max_per_row; $params[0]['after_widget'] = $params[0]['after_widget'] . '</div>'; }
	if ($col === 1) { $params[0]['before_widget'] = '<div class="footer-row">' . $params[0]['before_widget']; }

	$row = floor(($widget_num - 1)/$max_per_row)+1;
	
	$params[0]['before_widget'] = str_replace('class="', 'class="widget-max-per-row-' . $max_per_row . ' widget-number-' . $widget_num . ' widget-col-' . $col . ' widget-row-' . $row . ' ', $params[0]['before_widget']);
	
    }
    return $params;
}
add_filter('dynamic_sidebar_params','bsc_footer_widgets');


/*
function custom_post_snapshot_summary() {
    register_post_type('my_custom_post_type', array(
	'labels' => array( 'name' => __( 'Snapshot summary' ) ),
	'public' => true
    ));
}
add_action('init','custom_post_snapshot_summary');
add_post_type_support( 'custom_post_snapshot_summary', 'post-formats' );
*/

function blogsixteen_setup() {
    /* load_theme_textdomain( 'blogsixteen-child', get_template_directory() . '/languages' ); */
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary Menu', 'blogsixteen-child' )
    ) );
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );
/*
    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
	'quote',
        'link',
    ) );
*/
    add_theme_support(
	'custom-header',
	apply_filters(
	    'blogsixteen_custom_header_args', array(
		'default-image' => get_template_directory_uri() . '/images/header.jpg',
		'width'         => '1200',
		'height'        => '800',
		'uploads'       => true,
	    )
	)
    );
}
add_action( 'after_setup_theme', 'blogsixteen_setup' );


/**
 * Fix inline CSS injected by parent theme via customizer.
 * Parent theme generates invalid CSS if variables are undefined.
 */
function bsc_remove_parent_customizer_css() {
    /* remove_action('customize_register', 'blogsixteen_register_theme_customizer'); */
    remove_action('wp_head', 'blogsixteen_customizer_css');
}
add_action('init','bsc_remove_parent_customizer_css');

function bsc_fix_parent_customizer_css() {
    $mods = get_option( "theme_mods_blogsixteen-child");

    $css = [
	'blogsixteen_body_color' => [
	    'body' => [
		[ 'background-color' => null ]
	    ]
	],
	'blogsixteen_link_color' => [
	    'a' => [
		[ 'color' => null ]
	    ]
	],
	'blogsixteen_text_color' => [
	    'body' => [
		[ 'color' => null ]
	    ],
	    '.main-navigation ul ul a' => [
		[ 'color' => null ]
	    ]
	],
	'header_textcolor' => [
	    'body' => [
		[ 'color' => null ]
	    ]
	],
	'header_image' => [
	    'body.home #masthead' => [
		[ 'background-image' => null ],
	    ]
	],
	'blogsixteen_headline_color' => [
	    'h1,h2,h3,h4,h5' => [
		[ 'color' => null ]
	    ]
	],
	'blogsixteen_main_color' => [
	    '.current_page_item a' => [
		[ 'color' => null ]
	    ],
	    'button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover' => [
		[ 'border-color' => null ],
		[ 'background-color' => null ]
	    ],
	],
	'blogsixteen_header_color' => [
	    '.site-title a, .site-description, .main-navigation a' => [
		[ 'color' => null ]
	    ],
	    '.site-description:after' => [
		[ 'border-color' => null ]
	    ]
	]
    ];

    // FIXME Should use wp methods to enqueue this etc

    echo '<style>' . "\n";
    foreach ($css as $i => $k) {
	$custom_option = isset($mods[$i]) ? esc_html($mods[$i]) : null;

	$ret = '';
	$prt = false;

	foreach ($k as $a => $b) {

	    $ret .= $a . '{' . "\n";
	    foreach ($b as $x => $y) {
		foreach ($y as $attr => $val) {
		    $val = $val ? $val : ($custom_option ? $custom_option : null);
		    if ($val) {
			if (preg_match('/image$/i',$attr)) { $val = 'url("' . $val . '")'; } // FIXME
			if (preg_match('/^([a-f0-9]{3}){1,2}$/i',$val)) { $val = '#' . $val; } // FIXME
			$ret .= ' ' . $attr . ':' . $val . ';' . "\n"; // . ' /' . '* ' . $i . ' : ' . $attr . ' *' . '/' . "\n";
			$prt = true;
		    }
		}
	    }
	    $ret .= '}' . "\n";
	}

	if ($prt) { echo $ret; }
    }
    echo '</style>' . "\n";

    /*
    $style['header_color'] = '' || esc_html(get_theme_mod('blogsixteen_header_color'));
    $style['image'] = '' || esc_html(get_theme_mod('blogsixteen_image'));
    $style['header_background_color'] = '' || esc_html(get_theme_mod('blogsixteen_header_background_color'));
    $style['menu_background'] = '' || esc_html(get_theme_mod('blogsixteen_menu_background'));
    $style['menu_color_active'] = '' || esc_html(get_theme_mod('blogsixteen_menu_color_active'));
    */
}
add_action('wp_head','bsc_fix_parent_customizer_css');

function bsc_enqueue_styles() {
    /* wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); */

    wp_dequeue_style('blogsixteen-style');
    wp_deregister_style('blogsixteen-style');

    wp_dequeue_style('blogsixteen-google-fonts');
    wp_deregister_style('blogsixteen-google-fonts');

    wp_enqueue_style('blogsixteen-child-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,700');

    if (locate_template('style-parent.min.css') != '') {
	wp_register_style('bsc-parent-minified', get_stylesheet_directory_uri() . '/style-parent.min.css', array(), '', 'all');
	wp_enqueue_style('bsc-parent-minified');
    }
    if (locate_template('style.min.css') != '') {
	wp_register_style('bsc-minified', get_stylesheet_directory_uri() . '/style.min.css', array(), '0.0.1', 'all');
	wp_enqueue_style('bsc-minified');
    }
}
add_action('wp_enqueue_scripts', 'bsc_enqueue_styles', 20);


function bsc_register_sidebars() {
    register_sidebar(
        array(
            'id' => 'bsc-frontpage-splash',
            'name' => __('Front page splash', 'bsc'),
            'description' => __('Appears on the front page immediately after the nav menu and before anything else.','bsc'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        )
    );
    register_sidebar(
        array(
            'id' => 'bsc-footer-3',
            'name' => __('Footer content', 'bsc'),
            'description' => __('Appears in the footer of every page. Associated CSS creates responsive rows of 3 widgets.','bsc'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        )
    );
    register_sidebar(
        array(
            'id' => 'bsc-notification-bar',
            'name' => __('Notification bar', 'bsc'),
            'description' => __('Appears at the very top of every page.','bsc'),
            'before_widget' => '<div id="notification-bar"><div class="notification-bar-inner"><aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside></div></div>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        )
    );
}
add_action('widgets_init', 'bsc_register_sidebars');


/* Remove height attribute from thumbnail images, and set width to 100% */
function bsc_munge_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/height=\"\d*\"\s/', "", $html );
    $html = preg_replace( '/width=\"\d*\"\s/', "width='100%'", $html ); // FIXME Should do this just with CSS
    return $html;
}
add_filter( 'post_thumbnail_html', 'bsc_munge_thumbnail_dimensions', 10, 3 );

/* Useful dashboard widget to display all draft posts. */
if (is_admin() && (current_user_can('editor') || current_user_can('administrator'))) {
    function list_pending_drafts() {
	$args = array(
	    'numberposts' => '10',
	    'order_by' => 'post_date',
	    'order' => 'DESC',
	    'post_status' => 'pending'
	);
	$recent_posts = wp_get_recent_posts($args, ARRAY_A);

	if (! $recent_posts) {
	    echo '<p>No drafts in pending status.</p>' . "\n";
	    return;
	}
	
	echo '<style>table.d { border-collapse: collapse; }.d td { border: 1px solid silver; padding: 4px; }.d tr {vertical-align: top;}.d thead {font-weight: bold;}</style>' . "\n";
	echo '<table class="d">' . "\n";
	echo '<thead><tr><td>ID</td><td>Title</td><td>Author</td></tr></thead>' . "\n";
	echo '<tbody>' . "\n";
	foreach ($recent_posts as $post) {
	    echo '<tr>' . "\n";
	    echo '<td><a href="' . get_edit_post_link($post['ID'],null) . '">' . $post['ID'] . '</a></td>' . "\n";
	    echo '<td>' . $post['post_title'] . '</td>' . "\n";
	    echo '<td>Author</td>' . "\n";
	    echo '</tr>' . "\n";
	}
	echo '</tbody>' . "\n";
	echo '</table>' . "\n";
	wp_reset_query();
    }
    
    function add_dashboard_widgets() {
	wp_add_dashboard_widget('list_pending_drafts','Pending drafts by other authors','list_pending_drafts');
    }
    add_action('wp_dashboard_setup','add_dashboard_widgets');
}


/**
 * Override parent theme's script enqueuing by using a higher priority.
 * Necessary to prevent parent pulling in redundant CSS, and instead
 * pull in minified version of parent's CSS, along with this child's
 * minified CSS.
 */

function bsc_remove_parent_script_enqueuing() {
    remove_action( 'wp_enqueue_scripts', 'blogsixteen_scripts' );
    remove_filter( 'wp_enqueue_scripts', 'blogsixteen_scripts' );
}
add_action('wp_load','bsc_remove_parent_script_enqueuing');
