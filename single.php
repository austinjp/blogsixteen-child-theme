<?php
/**
 * @package BlogSixteenChild
 */

get_header();?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
                    while ( have_posts() ) {
			the_post();
			get_template_part( 'template-parts/content', 'single' );
			the_post_navigation();
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
			    comments_template();
			}
		    }
                ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
global $post;
if (null !== $post) {
    $disableSidebar = get_post_meta($post->ID, 'disableSidebar', $single = true);
    if ($disableSidebar !== 'true') {
	get_sidebar();
    }
}
?>
<?php get_footer(); ?>
