<?php
/**
 * @package BlogSixteenChild
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div id="wrap-all-but-footer">

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'blogsixteen' ); ?></a>

        <?php
          if (function_exists(dynamic_sidebar('bsc-notification-bar'))) {
            dynamic_sidebar('bsc-notification-bar');
          }
        ?>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
                  <hgroup>
                    <div class="logo">
                    <?php
                      if(get_theme_mod('blogsixteen_logo')) {
                        ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                          <img src="<?php echo esc_url( get_theme_mod( 'blogsixteen_logo' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                        </a><?php
                      }
                    ?>
                    </div>
                    <div class="site-info">
                      <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                      <p class="site-description"> <?php bloginfo( 'description' ); ?> </p>
                    </div>
                  </hgroup>
<!--
			<?php if ( get_theme_mod( 'blogsixteen_logo' ) ) : ?>
				  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				    <img src="<?php echo esc_url( get_theme_mod( 'blogsixteen_logo' ) ); ?>" width="250px" height:"auto" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
				  </a>
				  <?php else : ?>
				  <hgroup>
				    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<p class="site-description"> <?php bloginfo( 'description' ); ?> </p>
					</hgroup>
			<?php endif; ?>
-->
		</div><!-- .site-branding -->
	</header><!-- #masthead -->

	<nav role="navigation" id="site-navigation" class="main-navigation" aria-label='<?php _e( 'Primary Menu ', 'blogsixteen' ); ?>'>
		<div class="header-menu">
			<h1 class="screen-reader-text"><?php _e( 'Primary Menu', 'blogsixteen' ); ?></h1>
			<?php wp_nav_menu( array( 'theme_location'=>'primary' ) ); ?>
		</div>
	</nav>

        <?php if (is_front_page()) { ?>
<div id="prelim-frame">
  <div id="prelim">
    <?php
        if (function_exists(dynamic_sidebar('bsc-frontpage-splash'))) {
            dynamic_sidebar('bsc-frontpage-splash');
        }
    ?>
  </div>
</div>
        <?php } ?>

	<div id="page" class="hfeed site">

	<div id="content" class="site-content">
